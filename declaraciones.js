//EN ES6 SE TRABAJAN CON SCOPES O ESPACIOS, LO QUE DEFINAS DENTRO DE UN IF O UNA FUNCION TRABAJARA SOLO DENTRO DE ESA FUNCION  O SCOPE.
// SE PUEDEN PONER DOS VARIABLES CON EL MISMO NOMBRE SI ESTAN EN DISTINTOS SCOPES.

//*****************************************                     NO SE PUEDE HACER                        *********************************************** no se puede redefinir una variable con el mismo nombre en el mismo scope
// let nombre = "juan"

// if(1===1){
//     console.log("un if normal");
// }

// let nombre = "guille"

// console.log(nombre);

//*****************************************                     COMO SI SE PUEDE HACER                       *********************************************** Se esta modificando el valor de la variable sin redefinirla
// let nombre = "juan"

// if(1===1){
//     console.log("un if normal");
// }

// nombre = "guille"

// console.log(nombre);

//*****************************************                     NO SE PUEDE HACER                        *********************************************** No encuentra la variable nombre dentro del scope del if

// let nombre = "juan"

// if(1===1){
//     console.log(nombre);
// }

// nombre = "guille"

// console.log(nombre);

//*****************************************                     COMO SI SE PUEDE HACER                       *********************************************** OPC1 Definimos de nuevo la variable que queremos usar en el interior del scope del if
// let nombre = "juan"

// if(1===1){
//     let nombre = "Pedro"
//     console.log(nombre);
// }

// nombre = "guille"

// console.log(nombre);

//*****************************************                     COMO SI SE PUEDE HACER                       *********************************************** OPC2 Definimos la variable como una constante permitiendo que su uso sea global
// const nombre = "juan"

// if(1===1){
//     console.log(nombre);
// }

// console.log(nombre);

//*****************************************                     NO SE PUEDE HACER                        *********************************************** No se puede redefinir el valor de una constante ni aunque este formada por objetos
// const Persona = {
//     nombre:"lucas",
//     apellido: "martin"
// };

// Persona = {
//     nombre:"lucas",
//     apellido: "juanito"
// }

// console.log(Persona)
//*****************************************                     COMO SI SE PUEDE HACER PERO NO ES RECOMENDABLE                  ***********************************************
const Persona = {
    nombre:"lucas",
    apellido: "martin"
};

Persona.nombre = "guille"

console.log(Persona)