

const saludo = "Hola mundo!";
//-----------------------------------------------------------------------   IMPORTANTE STARTSWITH, ENDSWITH O INCLUDES y REPEAT  SON SOLO PARA STRINGS y son CASESENSITIVE

console.log(numero.startsWith("1"));
console.log( saludo.substr(0,1) === "h");   // version clasica de JS

console.log(saludo.startsWith("h"));        // version Moderna de ES6
console.log(saludo.startsWith("Mu",5));     //La segunda posicion indica desde donde empieza a contar o ver si empieza por algo, se empieza a contar desde el 1 no desde el 0
console.log(saludo.endsWith("!"));          // version Moderna de ES6

console.log(saludo.indexOf("a"));           // version clasica de JS
console.log(saludo.includes("a"));          // version Moderna de ES6  
console.log(saludo.includes("a",5));        // version Moderna de ES6  En este caso es lo mismo que el start y el end pero revisa si esta la letra a partir de la posicion que indiquemos.


console.log(saludo.repeat(3));              //Repite la frase las veces que indiquemos.


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//...............................................    STRING LITERAL  ...........................................// 
function obtenerNombre(){
    return "Guillermo Lucia";
}

const nombre = "guillermo";
const apellido = "Lucia";

let nombreCompleto = nombre + " " + apellido;
console.log(nombreCompleto);

let nombreCompleto1 = `El nombre completo es ${nombre} + ${apellido}`
console.log(nombreCompleto1);


let nombreCompleto2 = `El nombre completo es ${obtenerNombre()}`;
console.log(nombreCompleto2)
 

let multiLinea = `<h1>Ejemplo de una linea</h1>
Ejemplo de linea2
${obtenerNombre()}
`
console.log(multiLinea);

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function etiqueta(literales,...valoresJS){  //Funcion a la que llama el TAG indicado en el let mensajeTag
    let resultado = "";
    console.log(literales);
    console.log(valoresJS);

    for (i=0;i<=valoresJS.length; i++){
        if(i === valoresJS.length){
            resultado += literales[i];
        }else{
            resultado += literales[i];
            resultado += valoresJS[i];
        }
    }
    console.log(resultado)
    return resultado;
}

let unidades = 5,
    costo = 10;


let mensajeTag = etiqueta `${unidades} de lapices cuestan ${unidades * costo} euros.`  //Estamos indicando antes del string literal `` un tag llamado "etiqueta" que llama a su vez a una funcion.


let mensaje = `Hola \n Mundo \\`,
    mensaje2 = String.raw `Hola \n Mundo \\`; // Datos en crudo
    // mensaje3 = String.raw `Hola \n Mundo \\`;        ESTE NO FUNCIONA LOS TAGS O ELEMENTO QUE INDICAMOS AL PRINCIPIO SOLO FUNCIONAN CON STRING LITERAL `` (en este caso el tag es String.raw)
console.log(mensaje);
console.log(mensaje2);