

function saludo(mensaje="hola mundo",tiempo=1500){   //Se puede definir un valor por defecto de los parametros
    setTimeout(()=>{
        console.log(mensaje)
    }, tiempo);
}

function saludar (fn = fnTemporal){
    console.log(typeof fn)
    if (typeof fn === "undefined"){
        console.error("Esto no es una funcion");
        return;
    }
    fn();

}


const fnTemporal = ()=>{
    console.log("hula mundo FN");
}

saludar();



////////////////////////////////////////////////////////////////////////////////////////////////////////////

//------------------------ LOS ARGUMENTOS DE UNA FUNCION NO PASAN LA INFORMACION DE LOS PARAMETROS OPCIONALES 

function suma(a,b){
    console.log(arguments)
}

function suma2(a="1",b="2"){
    console.log(arguments)
}

suma(1,2,3,4);  //En este caso arguments mostrara todo lo que le pasemos a la funcion suma es decir 1,2,3,4 aunque solo hayamos definido dos valores como parametros.
suma2(); //En este caso arguments no mostrara nada por que no le hemos pasado nada aunque se le ponga por defecto un valor a los parametros de la funcion.

/////////////////////////////////////////////////////////////////////////////////////////////////////////////

// function agregar_alumno(){

// }

